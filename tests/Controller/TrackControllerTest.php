<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TrackControllerTest extends WebTestCase
{
    public function testshow()
    {
        $client = static::createClient();

        $client->request('GET', '/api/track/1098765432');
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
       
    }
    
   public function testchangeStorage()
    {
        $client = static::createClient();

        $client->request('GET', '/api/storage/database');
        
        // Test if Content-Type is valid application/json
        $this->assertTrue($client->getResponse()->headers->contains(
            'Content-Type', 'application/json'
        ));
        // Test if data storage was changed
        $this->assertStringContainsString('{"type":"success"', $client->getResponse()->getContent());
     
       
    }
    
}
