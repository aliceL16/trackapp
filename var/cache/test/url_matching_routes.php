<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/([^/]++)?(*:17)'
                .'|/api/(?'
                    .'|track/([^/]++)(*:46)'
                    .'|storage/([^/]++)(*:69)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        17 => [[['_route' => 'home', 'reactRouting' => null, '_controller' => 'App\\Controller\\TrackController::index'], ['reactRouting'], null, null, false, true, null]],
        46 => [[['_route' => 'tracking_show', '_controller' => 'App\\Controller\\TrackController::show'], ['no'], null, null, false, true, null]],
        69 => [
            [['_route' => 'change_storage', '_controller' => 'App\\Controller\\TrackController::changeStorage'], ['param'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
