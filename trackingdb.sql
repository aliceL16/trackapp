-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: dec. 03, 2020 la 10:40 PM
-- Versiune server: 10.4.14-MariaDB
-- Versiune PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `trackingdb`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `track`
--

CREATE TABLE `track` (
  `id` int(11) NOT NULL,
  `tracking_no` varchar(50) NOT NULL,
  `tracking_data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `track`
--

INSERT INTO `track` (`id`, `tracking_no`, `tracking_data`) VALUES
(1, '123456789', '2017-08-12 12:00:00'),
(2, '198765432', '2017-08-13 14:00:00'),
(3, '1098765432', '2016-11-21 23:00:00');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `track`
--
ALTER TABLE `track`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
