// ./assets/components/Track.js
    
import React, {Component} from 'react';
import axios from 'axios';

class Track extends Component {
     constructor(props) {
        super(props);
        this.state = {
            tracking_no: '',
            Message: '',
            type: '',
 
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({
            tracking_no: e.target.value,
            Message: '',
        });
    }
    
    handleSubmit(e) {
        e.preventDefault();
        
         axios({
            method: "GET", 
            url:'http://localhost:8001/api/track/'+this.state.tracking_no, 

          }).then((response)=>{
           
           this.setState({
                    Message: response.data.data,
                   type : "text-"+response.data.type,
                });
        });
     
    }
    render() {
        return (
            <div className="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <main role="main" className="inner cover">
                  <h1 className="cover-heading">Tracking Info.</h1>
                  <p className="lead">Please enter a tracking number below :</p>
                 <form className="form-inline" onSubmit={this.handleSubmit} method="GET">
                        <div className="form-group mx-sm-3 mb-2">
                          <input type="text" name='tracking_no'  onChange={this.handleChange}  className="form-control"  placeholder="Tracking No" />
                        </div>
                        <button type="submit" className="btn btn-default mb-2">Check</button>
                </form>       
                
                </main>
                <span className={this.state.type}>{this.state.Message}</span>
            </div>
        )
    }
}
    
export default Track;
