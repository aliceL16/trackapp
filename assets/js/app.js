/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router } from 'react-router-dom';
//import '../css/style.css';
import Track from './components/Track';
    
ReactDOM.render(<Router><Track /></Router>, document.getElementById('root'));