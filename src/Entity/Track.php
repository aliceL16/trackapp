<?php

namespace App\Entity;

use App\Repository\TrackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrackRepository::class)
 */
class Track
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $tracking_no;

    /**
     * @ORM\Column(type="datetime")
     */
    private $tracking_data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrackingNo(): ?string
    {
        return $this->tracking_no;
    }

    public function setTrackingNo(string $tracking_no): self
    {
        $this->tracking_no = $tracking_no;

        return $this;
    }

    public function getTrackingData(): ?\DateTimeInterface
    {
        return $this->tracking_data;
    }

    public function setTrackingData(\DateTimeInterface $tracking_data): self
    {
        $this->tracking_data = $tracking_data;

        return $this;
    }
}
