<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Track;
use Symfony\Component\Finder\Finder;


class TrackController extends AbstractController
{
     /**
     * @Route("/{reactRouting}", name="home", defaults={"reactRouting": null})
     */
    public function index()
    {
        return $this->render('track/index.html.twig');
    }
    
     /**
     * @Route("/api/track/{no}", name="tracking_show")
      *@return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function show(string $no): Response
    {        
        $data_Storage_type = $this->getStorage();
        $data = NULL;
       
        if($data_Storage_type == "database"){
            $repository = $this->getDoctrine()->getRepository(Track::class);
            $track_info = $repository->findOneBy(['tracking_no' => $no]);
            if ($track_info) {
                $data =  $track_info->getTrackingData()->format('Y-m-d H:i:s');
            }
        }else{
            $data = $this->getTrackInfo($no);
        }
        
        
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        if (is_null($data)) {
            $response_data = array("type" => "danger","data"=> 'No tracking data found for tracking number '.$no);
        }else{
              $response_data = array("type" => "success","data"=> ' Estimated delivery date : '. $data);
        }
        $response->setContent(json_encode($response_data));
        
        return $response
        ;
    }
    
      /**
     * @Route("/api/storage/{param}", name="change_storage")
      *@return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function changeStorage(string $param): Response
    {
        $finder = new Finder();
        $written = false;
        $resourceDir = realpath(__DIR__ . '/../../Resources');
        
        $finder->files()->name("*.json")->in($resourceDir);

        foreach ($finder as $file) {
            $contents = $file->getContents();
            $json =  json_decode($contents, TRUE);
            if($json['storage'] !== $param){
                $json['storage'] = $param;
                $written = file_put_contents($file, json_encode($json,true) );
            }
            

        }
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

            if ($written) {
                $response_data = array("type" => "success","data"=> 'Storage type changed!');
            }else{
                $response_data = array("type" => "error","data"=> 'Nothing to change');
            }
            
        $response->setContent(json_encode($response_data));
       return $response;
    }
    
 
    public function getStorage(){
        
        $storage_type = "database";
        $finder = new Finder();
        
        $resourceDir = realpath(__DIR__ . '/../../Resources');
        
        $finder->files()->name("*.json")->in($resourceDir);

        foreach ($finder as $file) {
            $contents = $file->getContents();
            $json =  json_decode($contents, TRUE);
            $storage_type = $json['storage'];
            
        }   
        return $storage_type;
    }
    
    public function getTrackInfo($param) {

        $resourceDir = realpath(__DIR__ . '/../../Resources/trackingdata.csv');

        $handle = fopen($resourceDir, "r");

        $lineNumber = 1;

        $data = NULL;
        // Iterate over every line of the file
        while (($raw_string = fgets($handle)) !== false) {
       
            $row = str_getcsv($raw_string);
            if (in_array($param, $row)) {
                $data = $row[2];
            }

            $lineNumber++;
        }

        fclose($handle);
        return $data;
    }
   

}
