### DATABASE ####

Pentru a avea acces la date, in root se gaseseste importul bazei de date, denumit "trackingdb.sql".
De asemenea, in folderul Resources se gaseste fisierul CSV si fisierul de cofigurare al mediului de stocare a datelor.
In fisierul ".env" se va modifica sectiunea  *** DATABASE_URL="mysql://track_app:96TR_D@127.0.0.1:3306/trackingDB?serverVersion=13" *** cu datele de configurare specifice server-ului pe care este
baza de date, respectiv username, parola, nume baza de date.


#### ENVIRONMENT ###
Pentru a testa aplicatia de frontend REACT cu SYMFONY :
 - Comanda "symfony server:start" se va executa dintr-un terminal deschis in radacina proiectului.
 - Intr-un alt terminal se executa comanda "yarn encore dev --watch" (acesta necesita yarn sa fie instalat) pentru a compila aplicatia React si a observa
	eventualele modificari/ erori asupra fisierelor de Javascript.
 - Intr-un browser se acceseaza adresa "localhost:8001" 
 
 